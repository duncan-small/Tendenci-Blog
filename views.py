from django.shortcuts import render
from django.http import HttpResponse
from tendenci.apps.pages.models import Page
from tendenci.apps.categories.models import Category
from tendenci.apps.categories.models import CategoryItem
from django.db.models import Count

def index(request,year='',month='',day=''):
    categories_list = Category.objects.order_by('name')
    category_instances = CategoryItem.objects.filter(category__in=categories_list).order_by('category')
    category_dict = {}
    for eachcategory in categories_list:
        category_dict[eachcategory] = CategoryItem.objects.filter(category=eachcategory).order_by('name').count()
    month_list = Page.objects.dates('create_dt','month')
    year_list = Page.objects.dates('create_dt','year')
    if day != '': pages_list = Page.objects.filter(create_dt__day=day,create_dt__month=month,create_dt__year=year).order_by('-create_dt')
    elif month != '': pages_list = Page.objects.filter(create_dt__month=month, create_dt__year=year).order_by('-create_dt')
    elif year != '': pages_list = Page.objects.filter(create_dt__year=year).order_by('-create_dt')
    else: pages_list = Page.objects.order_by('-create_dt')
    return render(request,'blog/index.html',{'category_dict':category_dict, 'year_list':year_list,'month_list':month_list,'categories_list':categories_list,'pages_list':pages_list,'view':'index'})
