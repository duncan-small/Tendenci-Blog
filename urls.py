from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^blog/$', views.index, name='blog.index'),
    url(r'^blog/(?P<year>\d{4})/$', views.index, name='blog.yearSort'),
    url(r'^blog/(?P<year>\d{4})/(?P<month>\d{2})/$', views.index, name='blog.monthSort'),
    url(r'^blog/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$', views.index, name='blog.daySort'),
]
